package com.concentrix.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Map;

public class JwtRbac {

    private final String audience;
    private final Algorithm algorithm;
    private final JWTVerifier verifier;

    private final PadlockService padlock;

    public JwtRbac(final String padlockUrl, final String audience) {
        this.audience = audience;

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(padlockUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        this.padlock = retrofit.create(PadlockService.class);

        this.algorithm = initAlgorithm();
        this.verifier = initVerifier();

    }

    public boolean hasTenant(final String tenantId, final String authToken) {
        final String jwt = parseAuthToken(authToken);
        final DecodedJWT decodedJWT = decodeJWT(jwt);
        final Map permissions = decodedJWT.getClaim("permissions").asMap();
        return permissions.containsKey(tenantId);
    }

    private static String parseAuthToken(final String authToken) {
        if (authToken == null || authToken.isEmpty())
            throw new RuntimeException("missing required 'bearer' authorization header");
        return authToken.replaceAll("(?i)bearer|\\s+", "");
    }

    private DecodedJWT decodeJWT(final String jwt) {
        return verifier.verify(jwt);
    }

    private Algorithm initAlgorithm() {
        final Response<String> publicKeyResponse;
        try {
            publicKeyResponse = padlock.publicKey().execute();
        } catch (final IOException ioe) {
            throw new RuntimeException(ioe);
        }

        final String publicKeyPEM = publicKeyResponse.body()
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replace("-----END PUBLIC KEY-----", "")
                .replaceAll("\\\\n", "");

        final byte[] publicKeyDER = Base64.getMimeDecoder().decode(publicKeyPEM);
        X509EncodedKeySpec spec = new X509EncodedKeySpec(publicKeyDER);

        final RSAPublicKey publicKey;
        try {
            final KeyFactory kFactory = KeyFactory.getInstance("RSA");
            publicKey = (RSAPublicKey) kFactory.generatePublic(spec);
        } catch (final NoSuchAlgorithmException | InvalidKeySpecException nsae) {
            throw new RuntimeException(nsae);
        }

        return Algorithm.RSA256(publicKey, null);
    }

    private JWTVerifier initVerifier() {
        return JWT.require(algorithm)
                .withIssuer("convergysiq")
                .withAudience(audience)
                .build();
    }
}
