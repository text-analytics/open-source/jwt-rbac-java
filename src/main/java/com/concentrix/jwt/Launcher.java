package com.concentrix.jwt;

public class Launcher {

    public static void main(final String[] args) {
        final JwtRbac jwtRbac = new JwtRbac("http://localhost:3000", "development");
        final String bearerToken = "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJkZXZlbG9wbWVudCIsImlzcyI6ImNvbnZlcmd5c2lxIiwibmJmIjoxNTQyMTI3MTUxLCJleHAiOjE1NDIyMTM1NTEsInN1YiI6IjU0Mjg5YjA5LTBhYTYtNDI1ZS05Zjk0LWQ1OTQyMjYxMWVlZCIsInBlcm1pc3Npb25zIjp7IjQ2YmU3YmZkLTdlZjgtNDJkMC1hODdhLWU4ODVhYjM2MWM5OCI6eyJsYWJlbCI6IlRlc3QgVGVuYW50IiwibmFtZSI6InRlc3RfdGVuYW50Iiwib3duZXIiOiI1NDI4OWIwOS0wYWE2LTQyNWUtOWY5NC1kNTk0MjI2MTFlZWQiLCJhbGwiOnsiYWxsIjoxNX19fSwiaWF0IjoxNTQyMTI3MTUxfQ.heG_YmKyZogtpI7nAXM6VZqIdPOh58Ue-RqzgYebblh8hlqXk93kz4FoyJtE7jOOq07jILWOuTq9RMn99SL16DrP076GQJSuWkRveUG76URTITKvLPkiJZIzfPoAzYmS_K0y7s3osvHAYBoGo8t59jmGh4q5bgX82o2WduozcAc";

        final boolean hasTenant = jwtRbac.hasTenant("46be7bfd-7ef8-42d0-a87a-e885ab361c98", bearerToken);

        System.out.println(hasTenant);
    }
}
