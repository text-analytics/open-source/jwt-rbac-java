package com.concentrix.jwt;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PadlockService {

    @GET("v0/tokens/public-key")
    Call<String> publicKey();
}
